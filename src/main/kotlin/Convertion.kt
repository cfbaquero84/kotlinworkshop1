/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    //convert of units of Metric System
    //Km to metters
    fun kmToM1(km: Double): Int {
        return 0
    }

    //Km to metters
    fun kmTom(km: Double): Double {
        return 0.0
    }

    //Km to cm
    fun kmTocm(km: Double): Double {
        return 0.0
    }

    //milimetters to metters
    fun mmTom(mm: Int): Double {
        return 0.0
    }

    //convert of units of U.S Standard System
    //convert miles to foot
    fun milesToFoot(miles: Double): Double {
        return 0.0
    }

    //convert yards to inches
    fun yardToInch(yard: Int): Int {
        return 0
    }

    //convert inches to miles
    fun inchToMiles(inch: Double): Double {
        return 0.0
    }

    //convert foot to yards
    fun footToYard(foot: Int): Int {
        return 0
    }

    //Convert units in both systems
    //convert Km to inches
    fun kmToInch(km: String?): Double {
        return 0.0
    }

    //convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {
        return 0.0
    }

    //convert yards to cm
    fun yardToCm(yard: String?): Double {
        return 0.0
    }
}